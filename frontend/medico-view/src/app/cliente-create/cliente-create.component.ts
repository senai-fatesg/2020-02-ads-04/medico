import { Component, OnInit } from '@angular/core';
import { ClienteServiceService } from '../cliente-service.service';

@Component({
  selector: 'app-cliente-create',
  templateUrl: './cliente-create.component.html',
  styleUrls: ['./cliente-create.component.css']
})
export class ClienteCreateComponent implements OnInit {

  public cliente = {nome: '', endereco: ''};

  constructor(private clienteService: ClienteServiceService) { }

  ngOnInit() {
  }
 
  salvar(){
    this.clienteService.post(this.cliente).subscribe(r => {this.cliente = {nome: '', endereco: ''}});
  }

}
