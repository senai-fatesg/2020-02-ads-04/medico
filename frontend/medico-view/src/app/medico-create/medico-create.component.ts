import { Component, OnInit } from '@angular/core';
import { MedicoServiceService } from '../medico-service.service';

@Component({
  selector: 'app-medico-create',
  templateUrl: './medico-create.component.html',
  styleUrls: ['./medico-create.component.css']
})
export class MedicoCreateComponent implements OnInit {

  public medico = {nome: '', crm: '', especialidade: ''};

  constructor(private medicoService: MedicoServiceService) { }

  ngOnInit() {
  }
 
  salvar(){
    this.medicoService.post(this.medico).subscribe(r => {this.medico = {nome: '', crm: '', especialidade: ''}});
  }

}
