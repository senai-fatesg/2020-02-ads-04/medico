import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteServiceService {

  constructor(private http: HttpClient) { }

  public get(): Observable<any> {
    return this.http.get("http://localhost:8080/cliente");
  }

  public post(cliente): Observable<any> {
    return this.http.post("http://localhost:8080/cliente", cliente);
  }

  public delete(id): Observable<any> {
    return this.http.delete(`http://localhost:8080/cliente/${id}`);
  }


}
