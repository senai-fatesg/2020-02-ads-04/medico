import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MedicoCreateComponent } from './medico-create/medico-create.component';
import { MedicoListComponent } from './medico-list/medico-list.component';
import { ClienteCreateComponent } from './cliente-create/cliente-create.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  {path: "", pathMatch: "full", redirectTo: "home"},
  {path: "home", component: HomeComponent},
  {path: "medico-create", component: MedicoCreateComponent},
  {path: "medico-list", component: MedicoListComponent},
  {path: "cliente-create", component: ClienteCreateComponent},
  {path: "cliente-list", component: ClienteListComponent},
  {path: "login", component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
