import { TestBed } from '@angular/core/testing';

import { MedicoServiceService } from './medico-service.service';

describe('MedicoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MedicoServiceService = TestBed.get(MedicoServiceService);
    expect(service).toBeTruthy();
  });
});
