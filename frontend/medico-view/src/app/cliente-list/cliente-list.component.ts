import { Component, OnInit } from '@angular/core';
import { ClienteServiceService } from '../cliente-service.service';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css']
})
export class ClienteListComponent implements OnInit {

  clientes = [];
  clienteSelecionado;

  constructor(private clienteService: ClienteServiceService) { }

  

  ngOnInit() {
    this.clienteService.get().subscribe(r => {this.clientes = r});
  }

  selectCliente(c){
    this.clienteSelecionado = c;
  }

  excluir(id){
    this.clienteService.delete(id).subscribe(r => {this.clienteService.get().subscribe(r => {this.clientes = r});})
  }
}
