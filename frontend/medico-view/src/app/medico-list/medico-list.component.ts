import { Component, OnInit } from '@angular/core';
import { MedicoServiceService } from '../medico-service.service';

@Component({
  selector: 'app-medico-list',
  templateUrl: './medico-list.component.html',
  styleUrls: ['./medico-list.component.css']
})
export class MedicoListComponent implements OnInit {

  medicos = [];
  medicoSelecionado;

  constructor(private medicoService: MedicoServiceService) { }

  

  ngOnInit() {
    this.medicoService.get().subscribe(r => {this.medicos = r});
  }

  selectMedico(m){
    this.medicoSelecionado = m;
  }

  excluir(id){
    this.medicoService.delete(id).subscribe(r => {this.medicoService.get().subscribe(r => {this.medicos = r});})
  }
}
