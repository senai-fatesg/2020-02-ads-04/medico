package com.franciscocalaca.medico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.medico.entity.ClienteClinica;

@Repository
public interface ClienteClinicaDao extends JpaRepository<ClienteClinica, Integer>{

}
