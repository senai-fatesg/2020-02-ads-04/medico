package com.franciscocalaca.medico.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.medico.entity.Medico;

@Repository
public interface MedicoDao extends JpaRepository<Medico, Long>{

}
