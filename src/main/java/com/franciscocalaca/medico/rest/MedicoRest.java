package com.franciscocalaca.medico.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.medico.dao.MedicoDao;
import com.franciscocalaca.medico.entity.Medico;

@RestController
@RequestMapping("/medico")
public class MedicoRest {

	@Autowired
	private MedicoDao medicoDao;
	
	@GetMapping
	public List<Medico> get(){
		return medicoDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody Medico medico) {
		medicoDao.save(medico);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Long id) {
		medicoDao.deleteById(id);
	}
	
}
