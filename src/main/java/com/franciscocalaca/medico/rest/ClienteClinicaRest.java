package com.franciscocalaca.medico.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.medico.dao.ClienteClinicaDao;
import com.franciscocalaca.medico.entity.ClienteClinica;

@RestController
@RequestMapping("/cliente")
public class ClienteClinicaRest {

	@Autowired
	private ClienteClinicaDao clienteClinicaDao;

	@PostMapping
	public void post(@RequestBody ClienteClinica clienteClinica) {
		this.clienteClinicaDao.save(clienteClinica);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.clienteClinicaDao.deleteById(id);
	}
	
	@GetMapping
	public List<ClienteClinica> get(){
		return this.clienteClinicaDao.findAll();
	}
	
}
